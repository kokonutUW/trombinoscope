---
# Complète ici tes informations au format YAML
# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/

# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Alfeze
nom: Ali

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---

# Profil

![Photo](https://framagit.org/aali/trombinoscope/-/raw/master/public/photo.webp) 

Je suis Alfeze Ali, un passionné d'informatique de 18 ans résidant à Nantes. Actuellement étudiant en informatique en première année de BTS SIO au *lycée La Colinière*, je suis animé par une passion pour l'informatique (cybersécurité) et les avancées technologiques.

## Contact
N'hésitez pas à me contacter :

- Adresse e-mail : aali.informatique@gmail.com
- Réseau professionnel : [LinkedIn](https://www.linkedin.com/in/alfeze-ali-121789292/)

## Formation
- **BTS SIO (Services Informatiques aux Organisations)**
  - **Année en cours : 2023-2025**
  - *Lycée La Colinière*, Nantes
  - *Spécialité :* SISR (Solution d'Infrastructure Système et Réseau)
  - *Option :* Maths approfondies

  
- **Baccalauréat STI2D (Sciences et Technologies de l'Industrie et du Développement Durable)**
  - **Année : 2021-2023**
  - Option Systèmes d'Information et Numérique (SIN)
  - *Lycée La Colinière*, Nantes

- **Seconde Générale et Technologique**
  - **Année : 2020**
  - *Lycée Nelson Mandela*, Nantes
  

## Compétences

| Domaine                  | Compétences                                |
|--------------------------|--------------------------------------------|
| Programmation            | Python, Arduino                            |
| Développement web        | HTML, CSS, Markdown                        |
| Systèmes d'exploitation  | Windows, Ubuntu, Xubuntu, Kali Linux       |                       
| Outils                   | Visual Studio Code, Git, Framagit          |
| Logiciel de virtualisation | VirtualBox, VMware                       |
| Langues                  | Français, Anglais, Arabe                   |


## Expériences

### Section informatique

- **Projets personnel**

    - **Environnement virtuel/Réseaux/Sécurité**

   - Description : À domicile, j'applique une approche proactive en créant des machines virtuelles pour réaliser des projets de réseaux et de pentest. Ce processus renforce ma conviction dans le choix de la spécialité SISR (Solution d'Infrastructure Système et Réseau), justifiant ainsi mon engagement envers la sécurité informatique et la gestion des infrastructures. Au cours de ces projets, j'ai relevé divers défis stimulants

### Divers

- **Récolte de Muguet (Emploi Saisonnier)**
  - *Les 3 Moulins*
  - **Dates : 19 avril 2023 - 28 avril 2023**
  - Description : J'ai participé à un emploi saisonnier de récolte de muguet, où j'ai travaillé avec une équipe pour récolter et préparer les brins de muguet. Cette expérience m'a permis d'acquérir des compétences en travail d'équipe, en organisation et en gestion du temps.

- **Stage d'Observation en Pharmacie**
  - *PHARMACIE DE LA REPUBLIQUE*, Nantes
  - **Dates : 16 décembre 2019 - 20 décembre 2019**
  - Description : J'ai effectué un stage d'observation en pharmacie où j'ai eu l'opportunité de découvrir le fonctionnement quotidien d'une pharmacie, d'observer les professionnels de la santé en action, et d'apprendre davantage sur les médicaments et les soins pharmaceutiques. Cette expérience m'a permis de développer une compréhension plus approfondie du secteur pharmaceutique.
