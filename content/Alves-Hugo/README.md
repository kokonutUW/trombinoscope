---
# Complète ici tes informations au format YAML

# un doute sur la syntaxe ? aide toi du site http://www.yamllint.com/
# Tes informations de profil (INFORMATIONS OBLIGATOIRES)
prenom: Hugo
nom: Alves

# écrit ci-dessous ta présentation en markdown
# un doute sur la syntaxe ? aide toi des ressources disponible sur moodle
---
  
# Profil  
![Moi Petit](https://media.discordapp.net/attachments/1140331122270666883/1154409279965630524/Nouveau_projet.jpg)  
ALVES Hugo  
17 ans  
Élève en BTS Services informatiques aux organisations au lycée la colinière à Nantes, passioné par le dévellopement et l'informatique en générale.  
  
## Contacts :  
Téléphone : [06.72.47.41.53](tel:+33672474153)  
Mail : [hugo.alves.pro1@gmail.com](mailto:hugo.alves.pro1@gmail.com)  
  
# Formations  
 - 2023-2024, **1ère année BTS SIO _(option envisagé : SLAM)_**, _Lycée la colinière à Nantes (44000)_  
 - 2020-2023, **Bac Professionnel Système Numérique option C (Système et réseaux)**, _Lycée Simone Veil à Noisiel (77186)_  
  
## Informatique :  
Experiences majeur qui m'ont fait choisir cette option et compétence dévellopé par cette même occasion :  

 - **Le dévellopement d'un serveur FiveM** (GTA RP : jeux vidéo de roleplay multijoueur)  
 -- Autodidaxie  
 -- Percéverence  

 - **Les jeux vidéo en générale**  
 -- Stratégie  
 -- Connaissance  
  
# Compétences  
| Compétences | Connaissancess |  
|--|--|  
| **Programmation** | HTML, Javascript, python |  
| **Système d'exploitation** | Windows, Ubuntu, Debian, Windows Server |  
| **Langues** | Français, Anglais, Espagnol |  
  
# Expériences  
 - 2022-2023, **Stage Cellule Réseau Etam Groupe**,  _Siège Etam Groupe à Clichy (92110)_  
 -- Création de réseaux pour de nouveaux magasins  
 -- Paramétrage de switch en agrégation pour des bornes wifi 2x2,5GB/s  
 -- Suppression de switch non nécéssaire  
  
 - 2022, **Stage Cellule Réseau Etam Groupe**,  _Siège Etam Groupe à Clichy (92110)_  
 -- Débrassage de tout les cables inutile brancher sur les switch (+désactivation des même ports)  
 -- Remplacement de switch disfonctionnels  
  
 - 2021, **Stage Partie Réseau Mairie de Montfermeil**, _Mairie de Montfermeil (93047)_  
 -- Création d'un server d'Hypervision "Centreon" sur Debian  
 -- Ajout des appareils sur le réseau dans "Centreon"  
 -- paramétrage de switch et bornes réseau  